﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Black_Jack
{
    struct Card
    {
        public string Name;
        public int Value;
        public Suit Suit;
    }

    enum Suit
    {
        Clubs,
        Diamonds,
        Hearts,
        Spades
    }   

    class Program
    {
        static void Main(string[] args)
        {
            Card[] cards = FillInCards();
            Card[] cardsCopy = (Card[])cards.Clone();

            Random rndm = new Random();
            Player user = new Player();
            Player computer = new Player();

            user.Name = "User";
            computer.Name = "Computer";
     
            user.Cards = new Card[0];
            computer.Cards = new Card[0];

            int firstCardReceiver = 0;        

            bool stop = false;
            bool externalStop = false;

            for (int i = 0; stop == false && externalStop == false; i++)
            {
                if (i == 0)
                {
                    Console.WriteLine("Enter 1 if you would like to recive card first, or 0 if you want computer to play first");

                    string input = Console.ReadLine();

                    externalStop = CheckForStop(input);
                    if (externalStop == true)
                        break;

                    firstCardReceiver = int.Parse(input);

                    if (firstCardReceiver == 1)
                    {
                        user.Turn = "first";
                        computer.Turn = "second";

                    }
                    else if (firstCardReceiver == 0)
                    {
                        computer.Turn = "first";
                        user.Turn = "second";
                    }

                    if (user.Turn == "first")
                    {
                        Console.WriteLine("Okey , you are first");

                        TwoCardsOutput userUpdate = GetTwoCardsInLine(user, cardsCopy);

                        user.Cards = userUpdate.newPlayerCards;
                        cardsCopy = userUpdate.newCopyCards;

                        TwoCardsOutput compUpdate = GetTwoCardsInLine(computer, cardsCopy);

                        computer.Cards = compUpdate.newPlayerCards;
                        cardsCopy = compUpdate.newCopyCards;
                    }
                    else
                    {
                        Console.WriteLine("Okey , computer recived cards first");

                        TwoCardsOutput compUpdate = GetTwoCardsInLine(user, cardsCopy);

                        computer.Cards = compUpdate.newPlayerCards;
                        cardsCopy = compUpdate.newCopyCards;

                        TwoCardsOutput userUpdate = GetTwoCardsInLine(user, cardsCopy);

                        user.Cards = userUpdate.newPlayerCards;
                        cardsCopy = userUpdate.newCopyCards;
                    }

                    user.DoSumPoints();
                    computer.DoSumPoints();

                    Console.WriteLine($"You have recived {user.Cards[0].Name} {user.Cards[0].Suit} and {user.Cards[1].Name} {user.Cards[1].Suit}");

                    Console.WriteLine("Would you like to recive one more card?enter yes/no");
     
                    string reciveNext = Console.ReadLine();

                    externalStop = CheckForStop(reciveNext);
                    if (externalStop == true)
                        break;

                    user = ReciveNextCard(user, reciveNext);
                    computer = ReciveNextCard(computer);

                    if (computer.RecieveNext == false && user.RecieveNext == false)
                    {
                        stop = true;

                        Console.WriteLine($"User Points is {user.Points}");
                        Console.WriteLine($"Computer Points is {computer.Points}");

                        Console.WriteLine("Game is ended");
                    }

                }

                if(user.RecieveNext == true)
                {
                    Card newCard = cardsCopy[rndm.Next(0, cardsCopy.Length)];
                    user.Cards = AddCards(user.Cards, newCard);

                    cardsCopy = ManageCards(cardsCopy, Array.IndexOf(cardsCopy, newCard));

                    Console.WriteLine($"You have recived {newCard.Name} {newCard.Suit}");
                    Console.WriteLine("Would you like to recive one more card?enter yes/no");

                    user.DoSumPoints();

                    string reciveNext = Console.ReadLine();

                    externalStop = CheckForStop(reciveNext);
                    if (externalStop == true)
                        break;

                    user = ReciveNextCard(user, reciveNext);
                }

                if (computer.RecieveNext == true)
                {
                    Card newCard = cardsCopy[rndm.Next(0, cardsCopy.Length)];
                    computer.Cards = AddCards(computer.Cards, newCard);

                    cardsCopy = ManageCards(cardsCopy, Array.IndexOf(cardsCopy, newCard));

                    computer.DoSumPoints();

                    computer = ReciveNextCard(computer);
                }
                     
                if (i > 0 && (computer.RecieveNext == false && user.RecieveNext == false) || user.Points == 21 || computer.Points == 21)
                {
                    stop = true;

                    Console.WriteLine($"User Points is {user.Points}");
                    Console.WriteLine($"Computer Points is {computer.Points}");

                    Player winner = WhoWon(user, computer);
                    
                    if(winner.Name == "User")
                    {
                        Console.WriteLine("User Won");
                        user.Wons++;
                    }else if (winner.Name =="Computer")
                    {
                        Console.WriteLine("Computer Won");
                        computer.Wons++;
                    }
                    else
                    {
                        Console.WriteLine("Same resault for oponents");
                    }
           
                    Console.WriteLine("Would you like to play more?enter yes/no");

                    string answer = Console.ReadLine();

                    externalStop = CheckForStop(answer);
                    if (externalStop == true)
                        break;

                    if (answer == "yes")
                    {
                        user.ResetPlayer();
                        computer.ResetPlayer();
                        stop = false;
                        cardsCopy = (Card[])cards.Clone();
                        i = -1;

                        Console.WriteLine("NEW GAME STARTED");
                    }
                    else
                    {
                        Console.WriteLine("Game is ended");
                    }
                }
            }

            Console.WriteLine($"GAME SCORE IS : COMPUTER WON {computer.Wons} TIMES , USER WON {user.Wons}");
            Console.WriteLine("BY BY");
            Console.ReadLine();
        }



        public static Card[] FillInCards()
        {
            Card[] cards = new Card[36];

            int index = 0;
            int num = 6;
            string[] pics = { "Ace,11", "King,4", "Lady,3", "Jack,2" };
            int picsIndex = 0;

            for (int i = 0; i < 9; i++)
            {
                if (num < 11)
                {
                    for (int j = 0; j < 4; j++)
                    {
                        cards[index].Name = num.ToString();
                        cards[index].Value = num;

                        switch (j)
                        {
                            case 0:
                                cards[index].Suit = Suit.Diamonds;
                                break;
                            case 1:
                                cards[index].Suit = Suit.Hearts;
                                break;
                            case 2:
                                cards[index].Suit = Suit.Clubs;
                                break;
                            case 3:
                                cards[index].Suit = Suit.Spades;
                                break;
                        }
                        index++;
                    }
                    num++;
                }
                else
                {
                    for (int j = 0; j < 4; j++)
                    {
                        cards[index].Name = pics[picsIndex].Split(',')[0];
                        cards[index].Value = int.Parse(pics[picsIndex].Split(',')[1]);

                        switch (j)
                        {
                            case 0:
                                cards[index].Suit = Suit.Diamonds;
                                break;
                            case 1:
                                cards[index].Suit = Suit.Hearts;
                                break;
                            case 2:
                                cards[index].Suit = Suit.Clubs;
                                break;
                            case 3:
                                cards[index].Suit = Suit.Spades;
                                break;
                        }
                        index++;
                    }
                    picsIndex++;
                }
            }
            return cards;
        }

        public static Card[] ManageCards(Card[] cards, int index)
        {
            Card[] newCards = new Card[cards.Length - 1];

            int j = 0;

            for (int i = 0; i < cards.Length; i++)
            {
                if (i == index)
                {
                    continue;
                }
                else
                {
                    newCards[j] = cards[i];
                    j++;
                }                   
            }
            return newCards;
        }

        public static Card[] AddCards(Card[] PrevCards,Card newCard)
        {
            Card[] newCards = new Card[1];
            if (PrevCards.Length < 1)
            {
                newCards[0] = newCard;
            }
            else
            {
                newCards = new Card[PrevCards.Length + 1];

                for (int i = 0; i < PrevCards.Length; i++)
                {
                    newCards[i] = PrevCards[i];
                }

                newCards[newCards.Length - 1] = newCard;
                return newCards;
            }
            return newCards;
        }

        public static Player ReciveNextCard(Player player, string answer = null)
        {          
            if (answer != null)
            {
                if (answer == "yes")
                {
                    player.RecieveNext = true;
                }
                else
                {
                    player.RecieveNext = false;
                }
            }
            else
            {
                if (player.Points < 18)
                {
                    player.RecieveNext = true;

                    Console.WriteLine("Computer want to recive one more card");
                }
                else
                {
                    player.RecieveNext = false;

                    Console.WriteLine("Computer do not want to recive one more card");
                }
            }
            Player newPlayer = player;
            return newPlayer;
        }

        public static TwoCardsOutput GetTwoCardsInLine(Player player, Card[] cardsCopy)
        {            
            Random rndm = new Random();

            Card[] newPlayerCards = new Card[0];
            Card[] newCopyCards = new Card[0];

            int j = 0;
            while (j < 2)
            {
                Card newCard = new Card();

                if (j == 0) 
                newCard = cardsCopy[rndm.Next(0, j == 0 ? cardsCopy.Length : newCopyCards.Length)];
                else
                newCard = newCopyCards[rndm.Next(0, j == 0 ? cardsCopy.Length : newCopyCards.Length)];

                newPlayerCards = AddCards(j == 0 ? player.Cards : newPlayerCards, newCard);

                newCopyCards = ManageCards(j== 0 ? cardsCopy : newCopyCards, Array.IndexOf(j == 0 ? cardsCopy : newCopyCards, newCard));
                j++;
            }
            return new TwoCardsOutput(newPlayerCards, newCopyCards);
        }

        public static Player WhoWon(Player player1, Player player2)
        {
            if (player1.Points == 21 && player2.Points == 21)
            {
                return new Player();
            }else if (player1.Points == player2.Points)
            {
                return new Player();
            }
            else if(player1.Points == 21)
            {
                return player1;
            }else if (player2.Points == 21)
            {
                return player2;
            }else if (player1.Points < 21 && player2.Points < 21)
            {
                if (player1.Points > player2.Points)
                    return player1;
                else
                    return player2;

            }else if(player1.Points > 21 && player2.Points > 21)
            {
                if (player1.Points < player2.Points)
                    return player1;
                else
                    return player2;
            }else if(player1.Points < 21 && player2.Points > 21)
            {
                if ((21 - player1.Points) < (player2.Points - 21))
                    return player1;
                else
                    return player2;
            }
            else
            {
                if ((21 - player2.Points) < (player1.Points - 21))
                    return player2;
                else
                    return player1;
            }

        }

        public static bool CheckForStop(string msg)
        {
            if (msg == "STOP")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public struct TwoCardsOutput
        {
            public Card[] newPlayerCards;
            public Card[] newCopyCards;

            public TwoCardsOutput(Card[] newPlayerCards, Card[] newCopyCards)
            {
                this.newPlayerCards = newPlayerCards;
                this.newCopyCards = newCopyCards;
            }
        }

        public struct Player
        {
            public string Name;
            public string Turn;
            public Card[] Cards;
            public int Points;
            public bool RecieveNext;
            public int Wons;

            public void DoSumPoints()
            {
                int sum = 0;

                for (int i = 0; i < Cards.Length; i++)
                {
                    sum += Cards[i].Value;
                }
                Points = sum;
            }
       
            public void ResetPlayer()
            {
                Points = 0;
                RecieveNext = false;
                Cards = new Card[0];
            }

            public Card[] GetCards()
            {
                return Cards;
            }
        }
    }
}
